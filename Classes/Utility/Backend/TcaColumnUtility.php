<?php
namespace Ajax\AjaxTtcontent\Utility\Backend;
use \TYPO3\CMS\Core\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Krzysztof Adamczyk <ka@t3log.pl>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class TcaColumnUtility
 *
 * @package Ajax\AjaxTtcontent\Utility\Backend
 */
class TcaColumnUtility {

	/**
	 * Optimizes further, adds custom fields for the project
	 *
	 * @return void
	 */
	public static function configureExntesionSpecificTableColumns() {
		// Add custom table columns, modifications, etc., here.
		// Logically group into separate functions.
		// ...
		self::extendTtContent();
	}

	/**
	 * Extends the tt_content table with project specific modifications
	 *
	 * @return void
	 */
	protected function extendTtContent() {
		// Custom Columns
		$ttContentColumns = array(
			'tx_ajaxttcontent_ajax' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:ajax_ttcontent/Resources/Private/Language/locallang_db.xlf:ajax',
				'config' => array(
					'type' => 'check',
				),
			)
		);
		Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $ttContentColumns, 1);
		Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'tx_ajaxttcontent_ajax', '', 'after:fe_group');
	}
}