<?php
namespace Ajax\AjaxTtcontent\UserFunc;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Krzysztof Adamczyk <ka@t3log.pl>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class Content
 *
 * @package Ajax\AjaxTtcontent\UserFunc
 */
class Content{
	/**
	 * @param $content
	 * @param $configuration
	 * @return string
	 */
	public function getTtContent($content, $configuration) {
		$res = array();
		$recordsArr = BackendUtility::getRecordsByField('tt_content', 'pid', $GLOBALS['TSFE']->id, "AND deleted = '0' AND hidden = '0' AND tx_ajaxttcontent_ajax='1'");
		$content = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
		if (is_null($recordsArr)) {
			return $res;
		}

		foreach ($recordsArr as $key => $row) {
			$cConf = array(
				'tables' => 'tt_content',
				'source' => ($row['uid']),
				'dontCheckPid' => 1,
			);
			$wrapClass = 'c' . $row['uid'];
			$res[] = array($wrapClass => $content->RECORDS($cConf));
		}
		return htmlspecialchars(json_encode($res), ENT_QUOTES, 'UTF-8');
	}
}

